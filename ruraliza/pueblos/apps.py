from django.apps import AppConfig


class PueblosConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pueblos'
