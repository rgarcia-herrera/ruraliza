# ruraliza

Revertir la tendencia extractivista que empobrece al campo y llena de gente las ciudades.



# Instalar

Como usa geodjango pues hay que instalar spatialite:

```
$ sudo apt install libsqlite3-mod-spatialite
```

Y estas bibliotecas que dice el manual:

```
$ sudo apt-get install binutils libproj-dev gdal-bin
```
